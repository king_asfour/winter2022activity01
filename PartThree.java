import java.util.Scanner;
public class PartThree{
	public static void main (String[] args){
		Scanner reader = new Scanner (System.in);
		AreaComputations ac = new AreaComputations();
		
		System.out.println("What is the length of the square?");
		int squareLength = reader.nextInt();
		int areaSquare = ac.areaSquare(squareLength);
		System.out.println("The area of the square is " + areaSquare);
		
		System.out.println("What is the length of the rectangle?");
		int rectangleLength = reader.nextInt();
		System.out.println("What is the width of the rectangle?");
		int rectangleWidth = reader.nextInt();
		int areaRectangle = ac.areaRectangle(rectangleLength,rectangleWidth);
		System.out.println("The area of the rectangle is " + areaRectangle);
	}
}