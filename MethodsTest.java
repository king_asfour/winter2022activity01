 public class MethodsTest{
	public static void main (String [] args){
		int x = 10;
		System.out.println(x);
		methodNoInputNoReturn();
		System.out.println(x);
		
		methodOneInputNoReturn(10);
		methodOneInputNoReturn(x);
		methodOneInputNoReturn(x + 50);
			
		methodTwoInputNoReturn(x,10.5);

		int z = methodNoInputReturnInt();
		System.out.println("The value of the variable Z is " + z);
		
		int value1 = 6;
		int value2 = 3;
		double squareRootOfValues = sumSquareRoot(value1,value2);
		System.out.println("The square root is: " + squareRootOfValues);
		
		
		String s1 = "hello";
		String s2 = "goodbye";
		
		System.out.println("length 1 is: " + s1.length());
		System.out.println("length 2 is: " + s2.length());
		
		System.out.println(SecondClass.addOne(50));
		SecondClass sc = new SecondClass();
		int newNumber = sc.addTwo(50);
		System.out.println("Second class addTwo method is: " + newNumber);
		
	}
	public static void methodNoInputNoReturn(){
		System.out.println("I'm in a method that takes no input and returns nothing");
		int x = 50;
		System.out.println(x);
	}
	public static void methodOneInputNoReturn(int x){
		System.out.println("Inside the method one input no return " + x);
	}
	public static void methodTwoInputNoReturn(int x, double y){
		System.out.println("Inside the method two input no return " + x + " " + y);
	}
	public static int methodNoInputReturnInt(){
		
		return 6;
	}
	public static double sumSquareRoot(int value1, int value2){
		
		int add = value1 + value2;
		double squareRoot = Math.sqrt(add);
		return squareRoot;
	}
}